using UnityEngine;

public class Moveblock : MonoBehaviour
{
    private GameObject cam;
    private float speedMultiplier = 1f;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("Main Camera");
        speedMultiplier = Random.Range(0.3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        ManageZAxis();
        ManageXAxis();
    }

    private void ManageXAxis()
    {
        if (transform.position.x >= cam.transform.position.x)
        {
            MoveMyself(-0.002f, 0f, 0f);
            return;
        }
        MoveMyself(0.002f, 0, 0);
    }

    private void ManageZAxis()
    {
        if (transform.position.z >= cam.transform.position.z)
        {
            MoveMyself(0, 0, -0.002f);
            return;
        }

        MoveMyself(0, 0, 0.002f);
    }

    private void MoveMyself(float x, float y, float z)
    {
        x *= speedMultiplier;
        y *= speedMultiplier;
        z *= speedMultiplier;
        Debug.Log(x + " x <-> y " + y + " y <-> z " + z);

        Vector3 position = transform.position;
        position.Set(position.x + x,  position.y + y, position.z + z);
        transform.position = position;
    }
}
