﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    public class PlayerXYZMove : MonoBehaviour
    {
        public int SPRINT_MULTIPLICATOR = 6;

        private readonly Dictionary<KeyCode, Action> keyEvents = new();

        public PlayerXYZMove()
        {
            keyEvents.Add(KeyCode.A, () => MoveCamera(-0.01f, 0f, 0f));
            keyEvents.Add(KeyCode.S, () => MoveCamera(0f, 0f, -0.01f));
            keyEvents.Add(KeyCode.W, () => MoveCamera(0f, 0f, 0.01f));
            keyEvents.Add(KeyCode.D, () => MoveCamera(0.01f, 0f, 0f));
        }

        // Update is called once per frame
        void Update()
        {
            foreach (KeyValuePair<KeyCode, Action> keyEvent in keyEvents)
            {
                if (Input.GetKey(keyEvent.Key))
                {
                    keyEvent.Value.Invoke();

                }
            }
        }
        private void MoveCamera(float x, float y, float z)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                x *= SPRINT_MULTIPLICATOR;
                y *= SPRINT_MULTIPLICATOR;
                z *= SPRINT_MULTIPLICATOR;
            }

            Vector3 position = transform.position;
            position.x += x;
            position.y += y;
            position.z += z;
            transform.position = position;
        }
    }
}